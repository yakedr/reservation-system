package ca.isucorp.reservations.exceptions;

public class ValidationObjectException extends RuntimeException {

    private final String messageCode;
    private final String defaultUserMessage;
    private final Long args;

    public ValidationObjectException(final Long id, String resourceName) {
        this.messageCode = "error.msg."+resourceName+".id.validation";
        this.defaultUserMessage = "Object Requested have validation errors";
        this.args = id;
    }
}
