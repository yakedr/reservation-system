package ca.isucorp.reservations.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class InUseExceptionMapper implements ExceptionMapper<ResourceInUseException> {

    @Override
    public Response toResponse(ResourceInUseException exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new EntityException(500, 0, exception.getLocalizedMessage(), null))
                .build();
    }
}
