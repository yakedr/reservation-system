package ca.isucorp.reservations.exceptions;

public class ResourceInUseException extends RuntimeException {

    private final String messageCode;
    private final String defaultUserMessage;
    private final Long args;

    public ResourceInUseException(final Long id, String resourceName) {
        this.messageCode = "error.msg."+resourceName+".id.used";
        this.defaultUserMessage = resourceName+" with identifier " + id + " is been used";
        this.args = id;
    }
}
