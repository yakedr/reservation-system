package ca.isucorp.reservations.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Procedure(name = "updateRanking")
    void updateRanking(@Param("value_") Integer value_, @Param("reservation_id") Long reservation_id);

    @Procedure(name = "updateFavorite")
    void updateFavorite(@Param("value_") Boolean value_, @Param("reservation_id") Long reservation_id);

}
