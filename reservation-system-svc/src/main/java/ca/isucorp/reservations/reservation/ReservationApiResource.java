package ca.isucorp.reservations.reservation;

import ca.isucorp.reservations.contact.ContactData;
import ca.isucorp.reservations.contact.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

@Path("/reservations")
@Component
public class ReservationApiResource {

    private final ReservationService reservationService;
    private final ContactService contactService;

    @Autowired
    public ReservationApiResource(ReservationService reservationService, ContactService contactService) {
        this.reservationService = reservationService;
        this.contactService = contactService;
    }

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response retrieveReservations(@Context UriInfo uriInfo) {
        String param = null;
        String direction = null;

        MultivaluedMap<String, String> parameters = uriInfo.getQueryParameters();

        int limit = Integer.valueOf(parameters.getFirst("limit"));
        int offset = Integer.valueOf(parameters.getFirst("offset"));

        if(parameters.containsKey("orderBy") && parameters.containsKey("direction")) {
            param = parameters.getFirst("orderBy");
            direction = parameters.getFirst("direction");
        }
        final Page<Reservation> reservations = this.reservationService.retrieveAllReservations(limit, offset, param, direction);
        return Response.status(Response.Status.OK).entity(reservations).build();
    }

    @GET
    @Path("{reservationId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response retrieveReservation(@PathParam("reservationId") final Long reservationId) {

        final ReservationData reservation = this.reservationService.retrieveReservation(reservationId);
        return Response.status(Response.Status.OK).entity(reservation).build();
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response createReservation(final ReservationData reservationData) throws Exception {
        ContactData contact = null;

        if(reservationData.getContactData() == null){
            throw new Exception("Contact Data must be present");
        }
        Reservation reservation = reservationData.toReservationEntity();
        contact = contactService.retrieveContact(reservationData.getContactData());

        reservation.setContact(contact.toContactEntity());
        Reservation result = this.reservationService.createReservation(reservation);

        return Response.status(Response.Status.CREATED).entity(result).build();
    }

    @PUT
    @Path("{reservationId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response updateReservation(@PathParam("reservationId") final Long reservationId, final Reservation reservationData) {

        Reservation result = this.reservationService.updateReservation(reservationData);

        return Response.status(Response.Status.OK).entity(result).build();
    }

    @PATCH
    @Path("{reservationId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response updatePartialReservation(@PathParam("reservationId") final Long reservationId, final Reservation reservationData) {

        this.reservationService.partialUpdateReservation(reservationData, reservationId);

        return Response.status(Response.Status.OK).build();
    }

    @DELETE
    @Path("{reservationId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response delete(@PathParam("reservationId") final Long reservationId) {

        Reservation result = this.reservationService.deleteReservation(reservationId);

        return Response.status(Response.Status.OK).entity(result).build();
    }
}