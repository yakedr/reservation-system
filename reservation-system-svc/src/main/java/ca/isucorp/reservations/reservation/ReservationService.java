package ca.isucorp.reservations.reservation;

import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Validated
public interface ReservationService {

    Page<Reservation> retrieveAllReservations(@Positive int limit, @PositiveOrZero int offset, String param, String direction);

    ReservationData retrieveReservation(Long reservationId);

    Reservation createReservation(Reservation reservationData);

    Reservation updateReservation(Reservation reservationData);

    void partialUpdateReservation(Reservation reservationData, Long reservationId);

    Reservation deleteReservation(Long reservationId);
}
