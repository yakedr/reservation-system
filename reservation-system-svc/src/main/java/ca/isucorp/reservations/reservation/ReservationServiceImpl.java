package ca.isucorp.reservations.reservation;

import ca.isucorp.reservations.contact.ContactRepository;
import ca.isucorp.reservations.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;
    private final ContactRepository contactRepository;

    @Autowired
    public ReservationServiceImpl(final ReservationRepository reservationRepository, ContactRepository contactRepository) {
        this.reservationRepository = reservationRepository;
        this.contactRepository = contactRepository;
    }

    @Override
    public Page<Reservation> retrieveAllReservations(int limit, int offset, String param, String direction) {

        Sort sort = new Sort(Sort.Direction.ASC, "destination");
        if (param != null && direction != null) {
            sort = new Sort(Sort.Direction.fromString(direction), param);
        }
        Page<Reservation> reservations = reservationRepository.findAll(PageRequest.of(offset, limit, sort));

        return reservations;
    }

    @Override
    public ReservationData retrieveReservation(Long reservationId) {

        Optional<Reservation> reservation = reservationRepository.findById(reservationId);
        if (reservation == null) throw new ResourceNotFoundException(reservationId, "reservation");
        return new ReservationData(reservation.get());
    }

    @Override
    public Reservation createReservation(Reservation reservation) {

        return this.reservationRepository.save(reservation);
    }

    @Override
    public Reservation updateReservation(Reservation reservationData) {

        final Optional<Reservation> reservation = this.reservationRepository.findById(reservationData.getId());
        if (reservation == null) {
            throw new ResourceNotFoundException(reservationData.getId(), "reservation");
        }

        Reservation toUpdate = reservation.get();
        toUpdate.setDescription(reservationData.getDescription());
        // toUpdate.setContact(reservationData.getContact());
        toUpdate.setDestination(reservationData.getDestination());

        return this.reservationRepository.saveAndFlush(toUpdate);
    }

    @Override
    public void partialUpdateReservation(Reservation reservationData, Long reservationId) {

        if (reservationData.getFavorite() != null) {
            this.reservationRepository.updateFavorite(reservationData.getFavorite(), reservationId);
        } else if (reservationData.getRanking() != null) {
            this.reservationRepository.updateRanking(reservationData.getRanking(), reservationId);
        }

    }


    @Override
    public Reservation deleteReservation(final Long reservationId) {

        final Optional<Reservation> reservationForDelete = this.reservationRepository.findById(reservationId);

        if (reservationForDelete == null) {
            throw new ResourceNotFoundException(reservationId, "reservation");
        }

        this.reservationRepository.delete(reservationForDelete.get());

        return reservationForDelete.get();
    }
}