package ca.isucorp.reservations.reservation;

import org.joda.time.LocalDate;

public class ReservationData {

    private Long id;
    private String destination;
    private String description;
    private String creationDate;
    private Boolean favorite;
    private Integer ranking;
    private Long contactData;

    public ReservationData() {
    }

    public ReservationData(Long id, String destination, String description, String creationDate, Boolean favorite, Integer ranking, Long contactData) {
        this.id = id;
        this.destination = destination;
        this.description = description;
        this.creationDate = creationDate;
        this.favorite = favorite;
        this.ranking = ranking;
        this.contactData = contactData;
    }

    public ReservationData(Reservation reservation) {
        this.id = reservation.getId();
        this.destination = reservation.getDestination();
        this.description = reservation.getDescription();
        this.creationDate = LocalDate.fromDateFields(reservation.getCreationDate()).toString();
        this.favorite = reservation.getFavorite();
        this.ranking = reservation.getRanking();
        this.contactData = reservation.getContact().getId();
    }

    public Reservation toReservationEntity(){
        return new Reservation(this.destination, this.description, LocalDate.parse(this.creationDate),
                this.favorite,this.ranking, null);
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Long getContactData() {
        return contactData;
    }

    public void setContactData(Long contactData) {
        this.contactData = contactData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
