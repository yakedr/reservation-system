package ca.isucorp.reservations.reservation;

import ca.isucorp.reservations.contact.Contact;
import org.joda.time.LocalDate;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "reservation")
@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(name = "updateRanking",
                procedureName = "updateRanking",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "value_", type = Integer.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "reservation_id", type = Long.class)
                }),
        @NamedStoredProcedureQuery(name = "updateFavorite",
                procedureName = "updateFavorite",
                parameters = {
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "value_", type = Boolean.class),
                        @StoredProcedureParameter(mode = ParameterMode.IN, name = "reservation_id", type = Long.class)
                })
})

public class Reservation {

    @Id
    @GeneratedValue
    private Long id;


    @Column(name = "destination", nullable = false)
    private String destination;

    @Lob
    @Column(name = "description", nullable = false, columnDefinition = "mediumtext")
    private String description;

    @Column(name = "creation_date", nullable = false)
    private Date creationDate;

    @Column(name = "favorite")
    private Boolean favorite;

    @Column(name = "ranking")
    private Integer ranking;

    @ManyToOne(optional = false)
    @JoinColumn(name = "contact_id")
    private Contact contact;

    public Reservation() {   }

    public Reservation(String destination, String description, LocalDate creationDate, Boolean favorite, Integer ranking, Contact contact) {
        this.destination = destination;
        this.description = description;
        this.creationDate = creationDate.toDateTimeAtStartOfDay().toDate();
        this.favorite = favorite;
        this.ranking = ranking;
        this.contact = contact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Boolean getFavorite() {
        return favorite;
    }

    public void setFavorite(Boolean favorite) {
        this.favorite = favorite;
    }

    public Integer getRanking() {
        return ranking;
    }

    public void setRanking(Integer ranking) {
        this.ranking = ranking;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
