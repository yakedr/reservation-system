package ca.isucorp.reservations.contact;

import org.joda.time.LocalDate;

import javax.validation.constraints.NotBlank;

public class ContactData {

    private long id;

    @NotBlank(message = "Contact name must not be blank!")
    private String name;
    @NotBlank(message = "Contact birth date must not be blank!")
    private String birthDate;
    private String phoneNumber;
    private long contactType;
    private String contactTypeName;

    public ContactData() {
    }

    public ContactData(long id, String name, String birthDate, String phoneNumber, long contactType, String contactTypeName) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.phoneNumber = phoneNumber;
        this.contactType = contactType;
        this.contactTypeName = contactTypeName;
    }

    public ContactData(Contact contact) {
        this.id = contact.getId();
        this.name = contact.getName();
        this.birthDate = LocalDate.fromDateFields(contact.getBirthDate()).toString();
        this.phoneNumber = contact.getPhoneNumber();
        this.contactType = contact.getContactType().getId();
        this.contactTypeName = contact.getContactType().getName();
    }

    public Contact toContactEntity(){
       return new Contact(this.id,this.name, LocalDate.parse(this.birthDate), this.phoneNumber,null);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public long getContactType() {
        return contactType;
    }

    public void setContactType(long contactType) {
        this.contactType = contactType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContactTypeName() {
        return contactTypeName;
    }

    public void setContactTypeName(String contactTypeName) {
        this.contactTypeName = contactTypeName;
    }
}