package ca.isucorp.reservations.contact;

import ca.isucorp.reservations.contactType.ContactType;
import ca.isucorp.reservations.contactType.ContactTypeRepository;
import ca.isucorp.reservations.exceptions.ResourceInUseException;
import ca.isucorp.reservations.exceptions.ResourceNotFoundException;
import ca.isucorp.reservations.exceptions.ValidationObjectException;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
public class ContactServiceImpl implements ContactService {

    private final static Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

    private final ContactRepository contactRepository;
    private final ContactTypeRepository contactTypeRepository;

    @Autowired
    public ContactServiceImpl(ContactRepository contactRepository, ContactTypeRepository contactTypeRepository) {
        this.contactRepository = contactRepository;
        this.contactTypeRepository = contactTypeRepository;
    }

    @Override
    public Page<Contact> retrieveAllContacts(int offset, int limit) {

        return contactRepository.findAll(PageRequest.of(offset,limit, new Sort(Sort.Direction.ASC,"name")));
    }

    @Override
    public Collection<ContactData> retrieveAllContacts() {

        List<Contact> contactList = contactRepository.findAll(new Sort(Sort.Direction.ASC, "name"));

        List<ContactData> collect = contactList.stream().map(c -> new ContactData(c)).collect(Collectors.toList());

        return collect;
    }

    @Override
    public Collection<ContactData> retrieveAllContactsByParam(String paramName, String value) {
        List<ContactData> collect;
        List<Contact> contactList = null;
        if(paramName.equals("name")) {
           contactList = contactRepository.findAllByNameIsContaining(value);
        }
          collect = contactList.stream().map(c -> new ContactData(c)).collect(Collectors.toList());

        return collect;
    }

    @Override
    public ContactData retrieveContact(Long contactId) {
            Optional<Contact>  contact = contactRepository.findById(contactId);

            if(contact.isPresent())
                return new ContactData(contact.get());

            throw new ResourceNotFoundException(contactId, "contact");
    }

    @Override
    public ContactData createContact(ContactData contactData) {

            final Optional<ContactType> contactType = contactTypeRepository.findById(contactData.getContactType());

            if(contactType.isPresent()){
                Contact contact = contactData.toContactEntity();
                contact.setContactType(contactType.get());

                return new ContactData(this.contactRepository.save(contact));
            }
             else throw new ResourceNotFoundException(contactData.getContactType(), "contactType");
    }

    @Override
    public ContactData updateContact(ContactData contactData) {


            Optional<Contact>  contact = this.contactRepository.findById(contactData.getId());
            if (!contact.isPresent()) { throw new ResourceNotFoundException(contactData.getId(), "contact"); }

            Contact toUpdate = contact.get();

            if(contactData.getContactType() != contact.get().getContactType().getId()){
                final Optional<ContactType> contactType = contactTypeRepository.findById(contactData.getContactType());
                toUpdate.setContactType(contactType.get());
            }
            try {
                toUpdate.setBirthDate(LocalDate.parse(contactData.getBirthDate()).toDate());
                toUpdate.setName(contactData.getName());
                toUpdate.setPhoneNumber(contactData.getPhoneNumber());
            }catch (IllegalArgumentException ex){
                throw new ValidationObjectException(contactData.getId(), "contact");
            }

            return new ContactData(this.contactRepository.saveAndFlush(toUpdate));
    }

    @Override
    public Long deleteContact(Long contactId) {

        Optional<Contact> contactForDelete = this.contactRepository.findById(contactId);

        if (!contactForDelete.isPresent()) { throw new ResourceNotFoundException(contactId, "contact"); }
        if(contactForDelete.get().getReservationList().size() > 0) {
            throw new ResourceInUseException(contactId, "contact");
        }

        this.contactRepository.delete(contactForDelete.get());

        return contactId;
    }

    @Override
    public Collection<ContactType> retrieveAllContactsTypes(){

        return contactTypeRepository.findAll(new Sort(Sort.Direction.ASC,"name"));
    }

}
