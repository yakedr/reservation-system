package ca.isucorp.reservations.contact;

import ca.isucorp.reservations.contactType.ContactType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Collection;

@Path("/contacts")
@Component
public class ContactApiResource {

    private final ContactService contactService;
    private static final Logger logger = LoggerFactory.getLogger(ContactApiResource.class);

    @Autowired
    public ContactApiResource(final ContactService contactService) {
        this.contactService = contactService;
    }

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response retrieveContacts(@Context UriInfo uriInfo) {

        logger.info("Starting retrieve all contacts");

        MultivaluedMap<String, String> parameters = uriInfo.getQueryParameters();
        if(parameters.containsKey("limit") && parameters.containsKey("offset")){
            int limit = Integer.valueOf(parameters.getFirst("limit"));
            int offset = Integer.valueOf(parameters.getFirst("offset"));

            final Page<Contact> result = this.contactService.retrieveAllContacts(offset, limit);
            logger.info("Building return object with " + result.getTotalElements() + " contacts");

            return Response.status(Response.Status.OK).entity(result).build();
        } else if(parameters.containsKey("name")) {
            String value = parameters.getFirst("name");
            final Collection<ContactData> result = this.contactService.retrieveAllContactsByParam("name", value);

            logger.info("Building return object with " + result.size() + " contacts");
            return Response.status(Response.Status.OK).entity(result).build();
        }

        final Collection<ContactData> result = this.contactService.retrieveAllContacts();
        logger.info("Building return object with " + result.size() + " contacts");
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @GET
    @Path("{contactId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response retrieveContact(@PathParam("contactId") final Long contactId) {

        logger.info("Starting retrieve contact with ID: " + contactId);

        final ContactData result = this.contactService.retrieveContact(contactId);

        logger.info("Building return object");
        return Response.status(Response.Status.OK).entity(result).build();
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response createContact(final ContactData contactData) {

        ContactData result = this.contactService.createContact(contactData);

        return Response.status(Response.Status.CREATED).entity(result).build();
    }

    @PUT
    @Path("{contactId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response updateContact(@PathParam("contactId") final Long contactId,final ContactData contactData) {

        ContactData result = this.contactService.updateContact(contactData);

        return Response.status(Response.Status.OK).entity(result).build();
    }

    @DELETE
    @Path("{contactId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response delete(@PathParam("contactId") final Long contactId) {

        Long result = this.contactService.deleteContact(contactId);

        return Response.status(Response.Status.OK).entity(result).build();
    }

    @GET
    @Path("/types")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response retrieveContactTypes() {

        final Collection<ContactType> result = this.contactService.retrieveAllContactsTypes();
        return Response.status(Response.Status.OK).entity(result).build();
    }
}