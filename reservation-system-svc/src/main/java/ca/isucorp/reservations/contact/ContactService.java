package ca.isucorp.reservations.contact;

import ca.isucorp.reservations.contactType.ContactType;
import org.springframework.data.domain.Page;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.Collection;

public interface ContactService {

    Page<Contact> retrieveAllContacts(@PositiveOrZero int offset, @Positive int limit);

    Collection<ContactData> retrieveAllContacts();

    Collection<ContactData> retrieveAllContactsByParam(@NotNull String paramName, @NotNull String value);

    ContactData retrieveContact(@Positive Long contactId);

    ContactData createContact(@NotNull ContactData contactData);

    ContactData updateContact(@NotNull ContactData contactData);

    Long deleteContact(@Positive Long contactId);

    Collection<ContactType> retrieveAllContactsTypes();
}
