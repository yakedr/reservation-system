package ca.isucorp.reservations.config;

import ca.isucorp.reservations.contact.ContactApiResource;
import ca.isucorp.reservations.reservation.ReservationApiResource;
import ca.isucorp.reservations.exceptions.GenericExceptionMapper;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

@Component
@ApplicationPath("/api/v1")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(ReservationApiResource.class);
        register(ContactApiResource.class);

        //CORS Support
        register(ResponseCorsFilter.class);

        //Exceptions Mappers
        register(GenericExceptionMapper.class);
    }
}
