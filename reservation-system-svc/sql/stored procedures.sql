-- ----------------------------
-- Procedure structure for reserv_by_contacts
-- ----------------------------
DROP PROCEDURE IF EXISTS `reserv_by_contacts`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `reserv_by_contacts`()
BEGIN
SELECT contact.name,count  FROM contact NATURAL
JOIN reservation
WHERE contact.id=reservation.contact_id
GROUP BY contact.name;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for updateFavorite
-- ----------------------------
DROP PROCEDURE IF EXISTS `updateFavorite`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateFavorite`(IN value_ NUMERIC, in reservation_id NUMERIC)
BEGIN
UPDATE reservation set favorite = value_ where reservation.id = reservation_id;
END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for updateRanking
-- ----------------------------
DROP PROCEDURE IF EXISTS `updateRanking`;
delimiter ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `updateRanking`(IN value_ NUMERIC, in reservation_id NUMERIC)
BEGIN
UPDATE reservation set ranking = value_ where reservation.id = reservation_id;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
