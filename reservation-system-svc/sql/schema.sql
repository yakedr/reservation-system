SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact`  (
  `id` bigint(20) NOT NULL,
  `birth_date` datetime NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone_number` varchar(255) DEFAULT NULL,
  `contact_type_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FK6dfda2c1owl8p09s48oeqsevo`(`contact_type_id`) USING BTREE,
  CONSTRAINT `FK6dfda2c1owl8p09s48oeqsevo` FOREIGN KEY (`contact_type_id`) REFERENCES `contact_type` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
);

-- ----------------------------
-- Table structure for contact_type
-- ----------------------------
DROP TABLE IF EXISTS `contact_type`;
CREATE TABLE `contact_type`  (
  `id` bigint(20) NOT NULL,
  `description` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
);
-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence`  (
  `next_val` bigint(20) DEFAULT NULL
);

-- ----------------------------
-- Table structure for reservation
-- ----------------------------
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation`  (
  `id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `description` mediumtext NOT NULL,
  `destination` varchar(255) NOT NULL,
  `favorite` bit(1) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL,
  `contact_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `FKh9qekoawefvwtaipfabwd19uf`(`contact_id`) USING BTREE,
  CONSTRAINT `FKh9qekoawefvwtaipfabwd19uf` FOREIGN KEY (`contact_id`) REFERENCES `contact` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
);

SET FOREIGN_KEY_CHECKS = 1;
