export class Reservation {

  id: number;
  destination?: string;
  description?: string;
  creationDate?: string;
  favorite?: boolean;
  ranking?: number;
  contactData: number;

  constructor() {
    this.description = '';
  }
}
