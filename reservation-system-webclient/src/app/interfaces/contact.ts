import {ContactType} from './contact-type';

export class Contact {

  id: number;
  name: string;
  phoneNumber: string;
  birthDate: string;
  contactType: number;
  contactTypeName?: string;

  constructor() {   }
}
