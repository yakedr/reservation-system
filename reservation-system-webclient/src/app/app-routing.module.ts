import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ReservationsListComponent} from './components/reservations-list/reservations-list.component';
import {ReservationFormComponent} from './components/reservation-form/reservation-form.component';
import {ContactFormComponent} from './components/contact-form/contact-form.component';

const routes: Routes = [
  { path: '', redirectTo: 'reservation-list', pathMatch: 'full'},
  { path: 'reservation-list', component: ReservationsListComponent },
  { path: 'reservation-add', component: ReservationFormComponent },
  { path: 'reservation-edit/:id', component: ReservationFormComponent },
  { path: 'contact-add', component: ContactFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
