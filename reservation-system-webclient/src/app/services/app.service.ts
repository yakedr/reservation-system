import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { configApp } from '../app.config';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class AppService {

  constructor(private http: HttpClient) { }

  /**
   *  Methods for Contacts
    */

  /**
   * List all contacts
   * @returns {Observable<Object>}
   */
  public getContacts() {
    const url = configApp.serverUrl + '/contacts';
    return this.http.get(url);
  }

  /**
   * Get contact by Id
   * @returns {Observable<Object>}
   */
  public getContact(id) {
    const url = configApp.serverUrl + '/contacts/' + id;
    return this.http.get(url);
  }

  /**
   * Get all contact types
   *
   * @returns {Observable<Object>}
   */
  public getContactTypes() {
    const url = configApp.serverUrl + '/contacts/types';
    return this.http.get(url);
  }

  /**
   * Create New Contact
   * @param contact
   * @returns {Observable<Object>}
   */
  public addContact(contact) {
    const url = configApp.serverUrl + '/contacts';
    const body = JSON.stringify(contact);
    return this.http.post(url, body, httpOptions);
  }

  /**
   * Update existing contact
   * @param contact
   * @returns {Observable<Object>}
   */
  public updateContact(contact) {
    const url = configApp.serverUrl + '/contacts/' + contact.id;
    const body = JSON.stringify(contact);
    return this.http.put(url, body, httpOptions);
  }

  /**
   * Delete existing contact
   * @param id
   * @returns {Observable<Object>}
   */
  public deleteContact(id) {
    const url = configApp.serverUrl + '/contacts/' + id;
    return this.http.delete(url, httpOptions);
  }

  /**
   * Get contacts by name
   * @param value
   * @returns {Observable<Object>}
   */
  public getContactsByName(value) {
    const url = configApp.serverUrl + '/contacts';
    const params = new HttpParams().set('name', value);

    return this.http.get(url, {params});
  }


  /**
   *  Methods for Reservations
   */

  /**
   * Create New Reservation
   * @param contact
   * @returns {Observable<Object>}
   */
  public addReservation(reservation) {
    const url = configApp.serverUrl + '/reservations';
    const body = JSON.stringify(reservation);
    return this.http.post(url, body, httpOptions);
  }

  /**
   * Update existing reservation
   *
   * @param reservation
   * @returns {Observable<Object>}
   */
  public updateReservation(reservation) {
    const url = configApp.serverUrl + '/reservations/' + reservation.id;
    const body = JSON.stringify(reservation);
    return this.http.put(url, body, httpOptions);
  }

  /**
   *  Update partial reservation, used for Modify Ranking and Add to Favorites
   * @param id
   * @param field
   * @returns {Observable<Object>}
   */
  public updatePartialReservation(id, field, value) {
    const url = configApp.serverUrl + '/reservations/' + id;
    const param = (field === 'ranking') ? {ranking : value} : {favorite : value};
    const body = JSON.stringify(param);
    return this.http.patch(url, body, httpOptions);
  }

  /**
   * List all reservations
   * @returns {Observable<Object>}
   */
  public getReservations(offset, limit, orderBy?, direction? ) {
    const url = configApp.serverUrl + '/reservations';
    let params = new HttpParams().set('offset', offset).set('limit', limit);

    if (orderBy != null && direction != null) {
      params = new HttpParams().set('offset', offset).set('limit', limit).
                                set('orderBy', orderBy).set('direction', direction);
    }
    return this.http.get(url, {params});
  }

  /**
   * Get reservation
   * @returns {Observable<Object>}
   */
  public getReservation(id ) {
    const url = configApp.serverUrl + '/reservations/' + id;
    return this.http.get(url);
  }
}
