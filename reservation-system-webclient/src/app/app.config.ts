import {environment} from '../environments/environment';

if (environment.production) {
  this.serverUrl = '';
} else {
  this.serverUrl = 'http://localhost:8081/api/v1';
}

export const configApp = {
  serverUrl: this.serverUrl,
  paginator: {
    limit: 5
  }
};
