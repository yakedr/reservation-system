import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule, MatInputModule, MatTableModule, MatPaginatorModule,
  MatIconModule, MatFormFieldModule, MatDatepickerModule, MatSelectModule, MatSortModule,
  MatListModule, MatExpansionModule } from '@angular/material';


@NgModule({
  imports: [
    CommonModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule
  ],
  exports: [
    MatButtonModule,
    MatListModule,
    MatFormFieldModule,
    MatIconModule,
    MatSelectModule,
    MatInputModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule

  ],
  declarations: []
})
export class MaterialModule { }
