import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ReservationsListComponent } from './components/reservations-list/reservations-list.component';
import { AppRoutingModule } from './app-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxLoremIpsumModule } from 'ngx-lorem-ipsum';
import { MaterialModule } from './material/material.module';
import { BarRatingModule } from 'ngx-bar-rating';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { ReservationFormComponent } from './components/reservation-form/reservation-form.component';
import { ContactFormComponent} from './components/contact-form/contact-form.component';
import { NumberOnlyDirective } from './directives/number.directive';
import { QuillModule } from 'ngx-quill';
import { AppService } from './services/app.service';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { TdDialogService } from '@covalent/core/dialogs';
import { CovalentDialogsModule } from '@covalent/core/dialogs';
import { ContactDataComponent } from './components/common/contact-data/contact-data.component';
import { MainBarComponent } from './components/common/main-bar/main-bar.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    ReservationsListComponent,
    ReservationFormComponent,
    ContactFormComponent,
    NumberOnlyDirective,
    ContactDataComponent,
    MainBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }), // Used for i18n
    BrowserAnimationsModule,
    ToastrModule.forRoot({positionClass: 'toast-top-right'}), // Show messages of error/success after service calls
    ToastContainerModule,
    MaterialModule, // Visual Components Library
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule, // Used for responsive
    NgxLoremIpsumModule, // Used for generate Lorem Ipsum paragraph
    BarRatingModule, // Used for render ranking stars bar
    NgxPaginationModule, // Used for pagination in main page
    QuillModule, // Component Rich Text Editor
    CovalentDialogsModule  // Visual Components Library
  ],
  providers: [AppService, TdDialogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
