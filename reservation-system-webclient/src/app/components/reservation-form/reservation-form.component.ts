import { Component, OnInit, ViewChild} from '@angular/core';
import * as moment from 'moment';
import { Contact } from '../../interfaces/contact';
import { Reservation } from '../../interfaces/reservation';
import { TranslateService } from '@ngx-translate/core';
import {ToastrService } from 'ngx-toastr';
import { AppService } from '../../services/app.service';
import { FormBuilder, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ContactDataComponent } from '../common/contact-data/contact-data.component';

/**
 * Component for Register Reservations, actions:
 *  Create, Edit, List
 */

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.scss']
})
export class ReservationFormComponent implements OnInit {

  displayedColumns = ['name', 'phoneNumber', 'birthDate', 'contactTypeName'];
  currentReservation = new Reservation();
  currentContact = new Contact();

  destination: FormControl;

  @ViewChild('contactData') contactData: ContactDataComponent;

  contactDataValid = false;
  mode = 'Add';

  constructor(private translate: TranslateService, private toastr: ToastrService, private appsvc: AppService,
              private formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
    this.initReservationForm();

    const currentPath = this.route.routeConfig.path;
    if (currentPath.includes('reservation-edit')) {
      this.route.paramMap.subscribe((params: ParamMap) => {
        const idParam = params.get('id');
        this.mode = 'Edit';
        this.appsvc.getReservation(idParam).subscribe(
          data => {
            this.currentReservation = data as Reservation;
            this.appsvc.getContact(this.currentReservation.contactData).subscribe(
              dataC => {
                this.currentContact = dataC as Contact;
              },
              err => {
                console.error(err);
                this.toastr.error(this.translate.instant('msg.error.getcontact'), this.translate.instant('msg.error.title'));
              }
            );
          },
          err => {
            console.error(err);
            this.toastr.error(this.translate.instant('msg.error.getreservation'), this.translate.instant('msg.error.title'));
          }
        );
      });
    }
  }

  /**
   * Initialize reactive form for Reservation Data
   */
  private initReservationForm(): void {
    this.destination = new FormControl('', [Validators.required]);
  }

  /**
   * Reset form and current reservation
   */
  resetData() {
    this.currentReservation = new Reservation();
    this.contactData.resetData();
  }

  /**
   * Validate minimun data required for create reservation
   */
  validData() {
    return this.contactData.contactForm.valid === true &&
          this.currentReservation.description != null &&
         (this.currentReservation.destination !== undefined &&
          this.currentReservation.destination.length > 0);
  }

  handleNextLocationEvent() {
    this.router.navigate(['reservation-list']);
  }

  handleContactFormStatus(event) {
    this.contactDataValid = event;
  }


 /**
   * Executing Calls to Service
   */

  /**
   * Create or Update Reservation
   */
  saveReservation() {
    this.currentReservation.creationDate = moment().format('YYYY-MM-DD').toString();
    this.contactData.validateContactForm();
    if (this.contactDataValid) {
      this.currentContact = this.contactData.getCurrentContact();
      this.currentReservation.contactData = this.currentContact.id;
      if (this.currentReservation.id > 0) {
        this.appsvc.updateReservation(this.currentReservation).subscribe(
          data => {
            this.resetData();
            this.toastr.success(this.translate.instant('msg.success.reservationupdated'),
              this.translate.instant('msg.success.title'));
            if (this.mode === 'Edit') {
              this.router.navigate(['reservation-list']);
            }
          },
          err => {
            console.error(err);
            this.toastr.error(this.translate.instant('msg.error.reservationupdate'), this.translate.instant('msg.error.title'));
          }
        );
      } else {
        this.appsvc.addReservation(this.currentReservation).subscribe(
          data => {
            this.resetData();
            this.toastr.success(this.translate.instant('msg.success.reservationcreated'),
              this.translate.instant('msg.success.title'));
          },
          err => {
            console.error(err);
            this.toastr.error(this.translate.instant('msg.error.reservationcreate'), this.translate.instant('msg.error.title'));
          }
        );
      }
    } else {
      this.toastr.error(this.translate.instant('msg.error.reservationcreate'), this.translate.instant('msg.error.title'));
    }
  }
}
