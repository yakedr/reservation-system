import {Component, OnInit} from '@angular/core';
import {Reservation} from '../../interfaces/reservation';
import * as moment from 'moment';
import {TranslateService} from '@ngx-translate/core';
import {configApp} from '../../app.config';
import {AppService} from '../../services/app.service';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';

@Component({
  selector: 'app-reservations-list',
  templateUrl: './reservations-list.component.html',
  styleUrls: ['./reservations-list.component.scss']
})
export class ReservationsListComponent implements OnInit {

  p = 1;  // Current page
  limit = configApp.paginator.limit;
  cantElements = 0; // Total of elements
  reservations: Reservation[] = [];
  selectedFilter;
  filters = [
    { value: 'DATE_ASC', code: 'filter.dateAsc', param: 'creationDate', direction: 'ASC' },
    { value: 'DATE_DESC', code: 'filter.dateDesc', param: 'creationDate', direction: 'DESC' },
    { value: 'ALPHA_ASC', code: 'filter.alphaAsc', param: 'destination', direction: 'ASC' },
    { value: 'ALPHA_DESC', code: 'filter.alphaDesc', param: 'destination', direction: 'DESC' },
    { value: 'RANKING_ASC', code: 'filter.rankingAsc', param: 'ranking', direction: 'ASC' },
    { value: 'RANKNG_DESC', code: 'filter.rankingDesc', param: 'ranking', direction: 'DESC' }
  ];

  constructor(private translate: TranslateService, private toastr: ToastrService, private appsvc: AppService, private router: Router) {
    this.getReservations(0, this.limit);
  }

  ngOnInit() { }

  loadPage(event) {
    this.p = event;
    if (this.selectedFilter != null) {
      this.getReservations(this.p - 1, this.limit, this.selectedFilter.param, this.selectedFilter.direction);
    } else {
      this.getReservations(this.p - 1, this.limit);
    }
  }

  handleNextLocationEvent() {
    this.router.navigate(['reservation-add']);
  }

  /**
   * Retrieve Reservations, after selected a sort priority
   */
  loadOrderedPage() {
    if (this.selectedFilter != null) {
      this.getReservations(this.p - 1, this.limit, this.selectedFilter.param, this.selectedFilter.direction);
    }
  }

  /**
   * Update Ranking value
   * @param event New ranking value
   * @param id
   */
  updateRanking(event, id) {
    this.updatePartialReservations(id, 'ranking', event);
  }

  /**
   * Update Favorite State
   * @param id
   * @param value
   */
  updateFavorite(id, value) {
    const newValue = (value == null || value === false) ? true : !value;
    this.updatePartialReservations(id, 'favorite', newValue);
  }

  /**
   * Retrieve Reservations
   * @param offset Page Index, start in 0
   * @param limit  Max cant of items per page
   * @param orderBy Field to order by
   * @param direction Ascendent (ASC) or Descendent (DESC)
   */
    getReservations(offset, limit, orderBy?, direction?) {
    this.appsvc.getReservations(offset, limit, orderBy, direction).subscribe(
      data => {
        this.reservations = data['content'] as Reservation[];
        this.cantElements = data['totalElements'];
      },
      err => {
        console.error(err);
        this.toastr.error(this.translate.instant('msg.error.getreservations'), this.translate.instant('msg.error.title'));
      }
    );
  }

  /**
   * Update Partially Reservations
   */
  updatePartialReservations(id, param, value) {
    this.appsvc.updatePartialReservation(id, param, value).subscribe(
      data => {
        if (param === 'favorite') {
          const index = this.reservations.findIndex(r => r.id === id);
          this.reservations[index].favorite = value;
        }
        this.toastr.success(this.translate.instant('msg.success.reservationupdated'), this.translate.instant('msg.success.title'));
      },
      err => {
        console.error(err);
        this.toastr.error(this.translate.instant('msg.error.reservationupdate'), this.translate.instant('msg.error.title'));
      }
    );
  }

}
