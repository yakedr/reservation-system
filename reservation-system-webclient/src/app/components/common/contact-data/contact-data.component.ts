import {
  AfterViewInit,
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
  SimpleChange,
  ViewChild,
  ViewContainerRef,
  Output, EventEmitter
} from '@angular/core';
import {Contact} from '../../../interfaces/contact';
import {ContactType} from '../../../interfaces/contact-type';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {LangChangeEvent, TranslateService} from '@ngx-translate/core';
import {configApp} from '../../../app.config';
import {ToastrService} from 'ngx-toastr';
import {AppService} from '../../../services/app.service';
import {TdDialogService} from '@covalent/core/dialogs';
import * as moment from 'moment';

@Component({
  selector: 'app-contact-data',
  templateUrl: './contact-data.component.html',
  styleUrls: ['./contact-data.component.scss']
})
export class ContactDataComponent implements OnInit, AfterViewInit, OnChanges {

  @Input() readonly: boolean; // Tells the component if must be load for Edition or Read Only

  @Input() displayedColumns; // Tells the component which column must be displayed on contact table

  @Input() currentContact?: Contact;

  @Output() isFormValid: EventEmitter<any> = new EventEmitter<any>(); // Emit event with current form status

  contactForm: FormGroup;
  contactTypes: ContactType[];
  filteredItems: ContactType[] = [];

  contacts: Contact[];
  dataSource: MatTableDataSource<Contact>;
  maxDate = moment().subtract(18, 'years'); // Validated for Age Majority

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('datatable') datatable: any;

  constructor(private formBuilder: FormBuilder, private translate: TranslateService,
              private appsvc: AppService, private toastr: ToastrService, private dialogsvc: TdDialogService,
              private vcr: ViewContainerRef) {
    this.getAllContacts();
    this.dataSource = new MatTableDataSource(this.contacts);

    if ( this.currentContact === undefined ) {
      this.currentContact = new Contact;
    }
  }

  ngOnInit() {

    this.initContactForm();
    this.getContactsTypes();

    this.paginator.pageSize = configApp.paginator.limit;
    this.updatePaginatorLang();
    this.translate.onLangChange.subscribe((event: LangChangeEvent) => {
      this.updatePaginatorLang();
    });
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnChanges(changes: SimpleChanges) {
    const contact: SimpleChange = changes.currentContact;
    if (contact !== undefined) {
      this.currentContact = contact.currentValue;
    }
    if (this.currentContact !== undefined) {
      this.initContactForm();
      this.loadContactData();
    }
  }

  /**
   *  Emit event with current form status
   */
  validateContactForm() {
    this.isFormValid.emit(this.contactForm.valid);
  }

  /**
   * Return current contact
   * @returns {Contact | undefined}
   */
  getCurrentContact() {
    return this.currentContact;
  }

  /**
   * Applying i18n to component mat-paginator
   */
  private updatePaginatorLang() {
    this.translate.get('label.items').subscribe((res: string) => {
      this.paginator._intl.itemsPerPageLabel = res;
    });
    this.translate.get('label.nextpage').subscribe((res: string) => {
      this.paginator._intl.nextPageLabel = res;
    });
    this.translate.get('label.previouspage').subscribe((res: string) => {
      this.paginator._intl.previousPageLabel = res;
      this.dataSource.paginator = this.paginator;
    });
  }

  /**
   * Initialize reactive form for Contact Data
   */
  private initContactForm(): void {
    this.contactForm = this.formBuilder.group({
      name: new FormControl('', [Validators.required]),
      type: new FormControl('', [Validators.required]),
      birthdate: new FormControl({value: '', disabled: this.readonly}, [Validators.required]),
      phonenumber: new FormControl('')
    });
  }

  /**
   * Return message for validation errors on form controls
   * @param controlName
   * @returns {string}
   */
  getErrorMessage(controlName) {
    let message = '';
    if (controlName === 'type' && this.contactForm.controls[controlName].hasError('required') ) {
      message = this.translate.instant('field.validation.type');
    } else {
      message = this.contactForm.controls[controlName].hasError('required') ? this.translate.instant('field.validation.required') :
        this.contactForm.controls[controlName].hasError('matDatepickerMax') ? this.translate.instant('field.validation.birthdate') :
          '';
    }
    return message;
  }

  /**
   * Reset form and current contact
   */
  resetData() {
    this.currentContact = new Contact();
    this.contactForm.reset();
  }

  /**
   * Load data of selected contact in form for edition or
   * @param id Id of selected contact
   */
  loadContactData(id?) {
    if (id != null) {
    const idx = this.contacts.findIndex(c => c.id === id);
    this.currentContact = this.contacts[idx];
  }
    this.contactForm.controls['name'].setValue(this.currentContact.name);
    this.contactForm.controls['phonenumber'].setValue(this.currentContact.phoneNumber);
    if (this.readonly) {
      this.contactForm.controls['type'].setValue(this.currentContact.contactTypeName);
    } else {
      this.contactForm.controls['type'].setValue(this.currentContact.contactType);
    }
    this.contactForm.controls['birthdate'].setValue(moment(this.currentContact.birthDate, 'YYYY-MM-DD'));
  }

  /**
   * Show dialog to confirm deletion of selected contact
   */
  confirmDeleteContact(id, name): void {
    this.dialogsvc.openConfirm({
      message:  this.translate.instant('msg.confirm.deleteContact', {'contactName': name}),
      disableClose: true,
      viewContainerRef: this.vcr,
      title: this.translate.instant('msg.confirm.title'),
      cancelButton: this.translate.instant('btn.cancel'),
      acceptButton: this.translate.instant('btn.accept'),
      width: '500px',
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.deleteContact(id);
        this.resetData();
        this.getAllContacts();
      }
    });
  }

  /**
   * Retrieve Contact Types, for select component
   */
  public getContactsTypes() {
    this.appsvc.getContactTypes().subscribe(
      data => {
        this.contactTypes = data as ContactType[];
      },
      err => {
        console.error(err);
        this.toastr.error(this.translate.instant('msg.error.getcontacttype'), this.translate.instant('msg.error.title'));
      }
    );
  }

  /**
   * Retrieve Contact, for mat-table component
   */
  public getAllContacts() {
    this.appsvc.getContacts().subscribe(
      data => {
        this.contacts = data as Contact[];
        this.dataSource.data = this.contacts;
      },
      err => {
        console.error(err);
        this.toastr.error(this.translate.instant('msg.error.getcontacts'), this.translate.instant('msg.error.title'));
      }
    );
  }

  /**
   * Create or Update a Contact
   * @param formValue Current values in form
   */
  saveContact(formValue) {
    if (this.contactForm.valid) {
    this.currentContact.name = formValue.name;
    this.currentContact.phoneNumber = formValue.phonenumber;
    this.currentContact.contactType = formValue.type;
    const birthdate = formValue.birthdate;

    if (birthdate != null) {
      this.currentContact.birthDate = birthdate.format('YYYY-MM-DD').toString();
    }

    if (this.currentContact.id > 0) {
      this.appsvc.updateContact(this.currentContact).subscribe(
        data => {
          this.resetData();
          this.getAllContacts();
          this.toastr.success(this.translate.instant('msg.success.contactupdated'),
            this.translate.instant('msg.success.title'));
        },
        err => {
          console.error(err);
          this.toastr.error(this.translate.instant('msg.error.contactupdate'), this.translate.instant('msg.error.title'));
        }
      );
    } else {
      this.appsvc.addContact(this.currentContact).subscribe(
        data => {
          this.resetData();
          this.getAllContacts();
          this.toastr.success(this.translate.instant('msg.success.contactcreated'),
            this.translate.instant('msg.success.title'));
        },
        err => {
          console.error(err);
          this.toastr.error(this.translate.instant('msg.error.contactcreate'), this.translate.instant('msg.error.title'));
        }
      );
    }
    }
  }

  /**
   * Delete selected contact
   * @param id ID of selected contact
   */
  deleteContact(id) {
    this.appsvc.deleteContact(id).subscribe(
      data => {
        this.resetData();
        this.getAllContacts();
        this.toastr.success(this.translate.instant('msg.success.contactdeleted'),
          this.translate.instant('msg.success.title'));
      },
      err => {
        console.error(err);
        this.toastr.error(this.translate.instant('msg.error.contactdelete'), this.translate.instant('msg.error.title'));
      }
    );
  }

  /**
   * Filter existing contacts by name
   * @param {String} value
   */
  findContact(value: String) {
    if ( value.length > 0) {
      this.appsvc.getContactsByName(value).subscribe(
        data => {
          const contacts = data as Contact[];
          this.filteredItems = contacts;
          if (contacts.length === 0) {
            this.resetData();
          }
        },
        err => {
          console.error(err);
          this.toastr.error(this.translate.instant('msg.error.reservationcreate'), this.translate.instant('msg.error.title'));
        }
      );
    } else {
      this.resetData();
      this.filteredItems = [];
    }
  }

  filterByName(searchText) {
    searchText = searchText.toLowerCase();
    return this.contacts.filter( it => {
      return it.name.toLowerCase().startsWith(searchText);
    });
  }

}
