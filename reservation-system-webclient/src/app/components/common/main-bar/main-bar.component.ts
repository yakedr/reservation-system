import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-bar',
  templateUrl: './main-bar.component.html',
  styleUrls: ['./main-bar.component.scss']
})
export class MainBarComponent implements OnInit {

  @Input() pageName: string;
  @Output() goNextLocation: EventEmitter<any> = new EventEmitter<any>();
  pageIndex: number;

  pages = [
    {name: 'AddReservation', pageTitle: 'label.reservationAdd',
      nextLocationLabel: 'label.reservationList', locationRoute: 'contact-add' },
    {name: 'ListReservation', pageTitle: 'label.reservationList',
      nextLocationLabel: 'btn.createReservation', locationRoute: 'reservation-add'},
    {name: 'ContactManager', pageTitle: 'label.contactManager',
      nextLocationLabel: 'label.reservationAdd', locationRoute: 'reservation-list'}
    ];

  constructor(private router: Router) {
  }

  ngOnInit() {
    this.getPage();
  }

  getPage() {
    this.pageIndex = this.pages.findIndex(p => p.name === this.pageName );
  }

  nextLocation() {
    this.goNextLocation.emit();
  }

}
