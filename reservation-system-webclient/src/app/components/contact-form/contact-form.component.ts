import {Component, OnInit, ViewContainerRef } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { TdDialogService } from '@covalent/core/dialogs';
import {Router} from '@angular/router';

/**
 * Component for Contact Manager, actions:
 *  Create, Edit, Delete, List
 */

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

  displayedColumns = ['name', 'phoneNumber', 'birthDate', 'contactTypeName', 'actions'];

  constructor(private translate: TranslateService,
              private router: Router, private dialogsvc: TdDialogService,
              private vcr: ViewContainerRef) {
    }

  ngOnInit() {
  }

  handleNextLocationEvent() {
    this.cancelActions();
  }

  /**
   * Show dialog to confirm exit from the page
   */
  cancelActions(): void {
    this.dialogsvc.openConfirm({
      message:  this.translate.instant('msg.confirm.cancelContactManager'),
      disableClose: true,
      viewContainerRef: this.vcr,
      title: this.translate.instant('msg.confirm.title'),
      cancelButton: this.translate.instant('btn.cancel'),
      acceptButton: this.translate.instant('btn.accept'),
      width: '500px',
    }).afterClosed().subscribe((accept: boolean) => {
      if (accept) {
        this.router.navigate(['reservation-add']);
      }
    });
  }
}
