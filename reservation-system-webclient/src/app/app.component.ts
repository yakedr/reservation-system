import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(private translate: TranslateService, private route: ActivatedRoute, private router: Router) {
    translate.setDefaultLang('es');
    translate.use('es');
  }

  switchLanguage(language: string) {
    this.translate.use(language);
  }

  isCurrentPath(url) {
    return this.router.isActive(url, true);
  }
}
